<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //masuk halaman register
    public function registerPage()
    {
        return view('halaman.daftar');
    }

    //masuk halaman welcome
    public function welcomePage(Request $request)
    {
        // dd($request->all());
        $first_nama = $request ["first_nama"];
        $last_nama = $request["last_nama"];
        $gender= $request["gender"];
        

        return view('halaman.home', compact('first_nama','last_nama','gender'));
    }
}
