<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Latihan laravel Puji</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->

    </head>
    <body>

    <h1><b>Buat Account Baru!</b></h1>
        <h3><b>Sign Up Form</b></h3>
            <form action="/welcome" method="post">
            @csrf
                First name:<br/><br/>
                <input type="text" name="first_nama"><br/><br/>

                Last name:<br/><br/>
                <input type="text" name="last_nama"><br/><br/>

                Gender:<br/><br/>
                <input type="radio" name="gender" value="Male"/>Male<br/>
                <input type="radio" name="gender" value="Female"/>Female<br/>
                <input type="radio" name="gender" value="Other"/>Other<br/><br/>

                Nationality:<br/><br/>
                <select>
                    <option>Indonesian</option>
                    <option>Jepang</option>
                    <option>Malaysia</option>
                </select><br/><br/>

                Language Spoken:<br/><br/>
                <input type="checkbox" name="indonesia">Bahasa Indonesia<br/>
                <input type="checkbox" name="english">English<br/>
                <input type="checkbox" name="other">Other<br/><br/>

                Bio:<br/><br/>
                <textarea rows="10" cols="30"></textarea>
                <br/>

                <input type="submit" value="Sign Up">

            </form>  

            </form>  

    </body>
</html>
